package com.shchipunov.workshop.controller;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.stereotype.Controller;

import com.shchipunov.workshop.domain.ChatMessage;

/**
 * @author Shchipunov Stanislav
 */
@Controller
public class ChatController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChatController.class);

	private final SimpUserRegistry userRegistry;

	@Autowired
	public ChatController(SimpUserRegistry userRegistry) {
		this.userRegistry = userRegistry;
	}

	/**
	 * Receives a message and broadcasts it
	 * 
	 * @param message
	 * @param principal
	 * @return
	 */
	@MessageMapping("/messages")
	public ChatMessage handleMessage(ChatMessage message, Principal principal) {
		message.setSender(principal.getName());
		LOGGER.debug("Content: {}", message.getContent());
		return message;
	}

	/**
	 * Sends a list of active users
	 * 
	 * @return
	 */
	@SubscribeMapping("/users")
	@SendTo("/topic/users")
	public List<String> getListOfActiveUsers() {
		return userRegistry.getUsers().stream()
				.map(SimpUser::getName)
				.collect(Collectors.toList());
	}

	/**
	 * Notifies about an exception
	 * 
	 * @param exception
	 * @return
	 */
	@MessageExceptionHandler
	@SendToUser("/queue/errors")
	public String handleException(Throwable exception) {
		return exception.getMessage();
	}
}

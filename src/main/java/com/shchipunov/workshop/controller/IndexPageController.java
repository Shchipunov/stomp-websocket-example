package com.shchipunov.workshop.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Shchipunov Stanislav
 */
@Controller
public class IndexPageController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IndexPageController.class);

	/**
	 * Shows index page
	 * 
	 * @return
	 */
	@GetMapping("/")
	public String showIndexPage() {
		LOGGER.debug("Welcome to STOMP WebSocket example!");
		return "indexPage";
	}

	/**
	 * Shows log in page
	 * 
	 * @return
	 */
	@GetMapping("/logIn")
	public String showLogInPage(Principal principal) {
		return principal != null ? "redirect:/" : "logInPage";
	}
}

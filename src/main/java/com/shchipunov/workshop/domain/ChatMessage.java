package com.shchipunov.workshop.domain;

/**
 * Presents a class that holds information about a chat message
 * 
 * @author Shchipunov Stanislav
 *
 */
public class ChatMessage {

	private String sender;
	private String content;

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}

package com.shchipunov.workshop;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

/**
 * Security configuration
 * 
 * @author Shchipunov Stanislav
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity webSecurity) {
		webSecurity.ignoring().antMatchers("/resources/static/**");
	}

	/**
	 * Main security configuration
	 */
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
			.headers()
				.addHeaderWriter(new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.SAMEORIGIN))
				.and()
			.authorizeRequests()
				.antMatchers("/").authenticated()
				.anyRequest().permitAll()
			.and().formLogin()
				.loginPage("/logIn")
				.usernameParameter("username")
				.passwordParameter("password")
				.loginProcessingUrl("/authenticate")
				.failureUrl("/logIn?error=true")
				.permitAll()
			.and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logOut"))
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.logoutSuccessUrl("/logIn")
			.and().csrf().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder
			.inMemoryAuthentication()
				.withUser("user_1").password("test").roles("USER")
				.and()
				.withUser("user_2").password("test").roles("USER");
	}

	/**
	 * Provides the new dialect (used in views)
	 */
	@Bean
	public SpringSecurityDialect springSecurityDialect() {
		return new SpringSecurityDialect();
	}
}

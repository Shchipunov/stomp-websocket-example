angular.module("chatApplication")
		.controller("chatController", function($scope, chatService) {
			$scope.messages = [];
			$scope.message = {};

			chatService.connect().then(function() {
				chatService.receive().then(null, null, function(message) {
					$scope.messages.push(message);
				});
				chatService.activeUsersStream().then(null, null, function(users) {
					$scope.users = users;
				});
				chatService.errorStream().then(null, null, function(error) {
					console.log(error);
				});
			});

			$scope.sendMessage = function() {
				chatService.send($scope.message);
				$scope.message = {};
			}
		});
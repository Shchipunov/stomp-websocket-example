angular.module("chatApplication")
		.factory("wrappedSocket", [ "$q", function($q) {
			var stompClient = null;
			var wrappedSocket = {
				connect : function(url) {
					return $q(function(resolve, reject) {
						stompClient = Stomp.over(new SockJS(url));
						stompClient.connect({}, function(frame) {
							resolve(frame);
						}, function(error) {
							reject("STOMP connection error: " + error);
						});
					});
				},
				disconnect : function() {
					if (stompClient != null) {
						stompClient.disconnect();
					}
				},
				subscribe : function(destination) {
					var deferred = $q.defer();
					if (stompClient == null) {
						deferred.reject("STOMP client not created!");
					} else {
						stompClient.subscribe(destination, function(message) {
							deferred.notify(JSON.parse(message.body));
						});
					}
					return deferred.promise;
				},
				subscribeOnce : function(destination) {
					return $q(function(resolve, reject) {
						if (stompClient == null) {
							reject("STOMP client not created!");
						} else {
							stompClient.subscribe(destination, function(message) {
								resolve(JSON.parse(message.body));
							});
						}
					});
				},
				send : function(destination, headers, message) {
					stompClient.send(destination, headers, JSON.stringify(message));
				}
			};
			return wrappedSocket;
		} ])
		.factory("chatService", [ "wrappedSocket", "$q", function(wrappedSocket, $q) {
			var chatService = {
				connect : function() {
					return wrappedSocket.connect("/chat");
				},
				disconnect : function() {
					wrappedSocket.disconnect();
				},
				receive : function() {
					return wrappedSocket.subscribe("/topic/messages");
				},
				send : function(message) {
					return wrappedSocket.send("/application/messages", {}, message);
				},
				activeUsersStream : function() {
					var promise = wrappedSocket.subscribe("/topic/users");
					wrappedSocket.subscribeOnce("/application/users");
					return promise;
				},
				errorStream : function() {
					return wrappedSocket.subscribe("/user/queue/errors");
				}
			};
			return chatService;
		} ]);